package com.apple.spinnersample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener {

    private ArrayList<Person> allPersons;
    private Spinner spinner;
    private Boolean spinnerLoaded = false; // When initializing a spinner, it automatically runs its onItemSelection method
    //so to run the method only when we click the item, this boolean is created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.spinner);


        allPersons = new ArrayList<>();
        //Add persons you get from jsonvalue here
        Person person1 = new Person(1,"Nabika");
        Person person2 = new Person(2,"Rikesh");
        allPersons.add(person1);
        allPersons.add(person2);

        ArrayAdapter<Person> adapter =
                new ArrayAdapter<Person>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, allPersons);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(spinnerLoaded){
            Person person = allPersons.get(position);
            Toast.makeText(MainActivity.this, person.getId() + "", Toast.LENGTH_SHORT).show();
        }
        spinnerLoaded = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
